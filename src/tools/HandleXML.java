package tools;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;

public class HandleXML {

	private String title = "title";
	private String dte = "date";
	private String description = "description";

	private String urlString = null;
	private XmlPullParserFactory xmlFactoryObject;
	public volatile boolean parsingComplete = true;

	public HandleXML(String url) {
		this.urlString = url;
	}

	public String getTitle() {
		return title;
	}

	public String getDate() {
		return dte;
	}

	public String getDescription() {
		return description;
	}

	public void parseXMLAndStoreIt(XmlPullParser myParser) {
		int event;
		String text = null;
		try {
			event = myParser.getEventType();
			while (event != XmlPullParser.END_DOCUMENT) {
				String name = myParser.getName();
				switch (event) {
				case XmlPullParser.START_TAG:
					break;
				case XmlPullParser.TEXT:
					text = myParser.getText();
					break;
				case XmlPullParser.END_TAG:
					if (name.equals(title)) {
						title = text;
					} else if (name.equals(dte)) {
						dte = text;
					} else if (name.equals(description)) {
						description = text;
					} else {
					}
					break;
				}
				event = myParser.next();
			}
			parsingComplete = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fetchXML() {

		try {
	         URL url = new URL(urlString);
	         HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	         conn.setReadTimeout(10000 /* milliseconds */);
	         conn.setConnectTimeout(15000 /* milliseconds */);
	         conn.setRequestMethod("GET");
	         conn.setDoInput(true);
	         // Starts the query
	         conn.connect();
	         InputStream is = conn.getInputStream();
	         
	         xmlFactoryObject = XmlPullParserFactory.newInstance();
	         XmlPullParser myparser = xmlFactoryObject.newPullParser();
	         myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
	         myparser.setInput(is, null);
	         parseXMLAndStoreIt(myparser);
	         is.close();
	      } catch (Exception e) {
	    		Log.e("Error Code 90", e.getMessage());
	      }
		
	}
}