package tools;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Environment;
import android.widget.EditText;
import android.widget.Toast;

public class PictureHandler implements PictureCallback {

	private Camera cameraObject = null;

	public void setCamera(Camera obCamera) {
		cameraObject = obCamera;
	}

	private String sFilename = null;
	private EditText oEdit = null;

	public void setFileName(EditText obj) {
		oEdit = obj;
	}

	public void setFileName(String obj) {
		sFilename = obj;
	}

	public String getFileSaved() {
		return sFilename;
	}

	private String sMessage = null;

	public String getMessage() {
		return sMessage;
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera) {

		Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		if (bitmap == null) {
			sMessage = "not taken";
		} else {
			sMessage = "taken";
		}

		FileOutputStream fileOutputStream = null;
		BufferedOutputStream bos = null;
		int quality = 100;

		String folderPath = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
				+ File.separator;

		File mediaFolder = new File(folderPath);

		if (mediaFolder.exists() == false) {
			mediaFolder.mkdir();
		}

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HHmm");

		sFilename = "file" + dateFormat.format(calendar.getTime()) + ".jpg";
		File mediaFile = new File(folderPath + sFilename);

		try {
			fileOutputStream = new FileOutputStream(mediaFile);
			bos = new BufferedOutputStream(fileOutputStream);
			bitmap.compress(CompressFormat.JPEG, quality, bos);

			if (bos != null) {
				bos.close();
			}

		} catch (FileNotFoundException e) {

		} catch (Exception e) {

		}

		oEdit.setText(getFileSaved());
		
		cameraObject.release();
		
		//renaming the textbox as a clear path
		

	}

}
