package main.layout;

import main.layout.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class RSSActivity  extends Activity {

	private Button btn_refrsh;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rss);
		
		 btn_refrsh = (Button) findViewById(R.id.btn_refresh);
		 btn_refrsh.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					// Launching News Feed Screen
					Intent i = new Intent(getApplicationContext(),
							RSS2Activity.class);
					startActivity(i);
				}
			});
	}

}
