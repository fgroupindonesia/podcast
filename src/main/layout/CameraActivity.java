package main.layout;

import main.layout.R;

import tools.PictureHandler;
import tools.ShowCamera;
import android.app.Activity;

import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class CameraActivity extends Activity {

	private Camera cameraObject;
	private ShowCamera showCamera;
	private ImageView pic;
	private PictureHandler capturedIt = new PictureHandler();
	private Button btn = null;
	private EditText editText1 = null;

	public static Camera isCameraAvailiable() {
		Camera object = null;
		try {
			object = Camera.open();
		} catch (Exception e) {
		}
		return object;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);

		btn = (Button) findViewById(R.id.btn_capture);
		
		editText1 = (EditText) findViewById(R.id.editText1);
		
		pic = (ImageView) findViewById(R.id.imageView1);
		cameraObject = isCameraAvailiable();
		
		capturedIt.setFileName(editText1);
		capturedIt.setCamera(cameraObject);
		
		showCamera = new ShowCamera(this, cameraObject);
		FrameLayout preview = (FrameLayout) findViewById(R.id.frame_picture);
		preview.addView(showCamera);
		
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				cameraObject.takePicture(null, null, capturedIt);
				cameraObject.release();
			}
		});

	}

}
