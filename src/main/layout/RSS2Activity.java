package main.layout;

import main.layout.R;

import tools.HandleXML;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

public class RSS2Activity extends Activity {

	//private String finalUrl = "http://10.0.2.2:88/andro/simplerss.xml";
	private String finalUrl = "http://192.168.1.103:88/andro/simplerss.xml";
	private HandleXML obj;
	private TextView ddate, dtitle;
	private EditText ddesc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rss2);

		ddate = (TextView) findViewById(R.id.txt_date);
		dtitle = (TextView) findViewById(R.id.txt_title);
		ddesc = (EditText) findViewById(R.id.edt_desc);

	}

	@Override
	protected void onStart() {
		super.onStart();
		this.fetch();
	}

	public void fetch() {
		
		try {
			
			obj = new HandleXML(finalUrl);
			obj.fetchXML();
			
			ddate.setText(obj.getDate());
			ddesc.setText(obj.getDescription());
			dtitle.setText(obj.getTitle());

		}  catch (Exception e){
			Log.e("Error Code 64", e.getMessage());
		}

	}

}
