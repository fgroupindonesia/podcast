package main.layout;

import main.layout.R;

import alljoyns.BusHandler;

import android.app.Activity;


import android.os.Bundle;
import android.os.HandlerThread;

import android.util.Log;
import android.widget.Toast;

public class Chat1Activity extends Activity {

	private BusHandler mBusHandler;
	
	static {
        System.loadLibrary("alljoyn_java");
    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat1);
		
		/* Make all AllJoyn calls through a separate handler thread to prevent blocking the UI. */
        HandlerThread busThread = new HandlerThread("BusHandler");
        busThread.start();
        mBusHandler = new BusHandler(busThread.getLooper(), this);

        /* Advertise & Discover */
        mBusHandler.advertiseAndDiscover();
        sendMessage("Starting");
        mBusHandler.connect();
		
	}
	
	 @Override
	 protected void onDestroy(){
		super.onDestroy();
		mBusHandler.disconnect();
	}
	
	public void sendMessage(String aMess){
		Toast.makeText(getApplicationContext(), aMess, Toast.LENGTH_LONG).show();
	}
	
	public void sendMessage(String aCode, Object val){
		//temporarily used
		Toast.makeText(getApplicationContext(), val.toString(), Toast.LENGTH_LONG).show();
	}

}
