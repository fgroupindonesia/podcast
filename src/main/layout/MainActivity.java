package main.layout;

import main.layout.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/**
		 * Creating all buttons instances
		 * */
		// Dashboard News feed button
		Button btn_newsfeed = (Button) findViewById(R.id.btn_feed);

		// Dashboard Camera button
		Button btn_camera = (Button) findViewById(R.id.btn_camera);

		// Dashboard Peers button
		Button btn_peers = (Button) findViewById(R.id.btn_peers);

		// Dashboard GPS button
		Button btn_gps = (Button) findViewById(R.id.btn_gps);

		// Listening to News Feed button click
		btn_newsfeed.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				Intent i = new Intent(getApplicationContext(),
						RSSActivity.class);
				startActivity(i);
			}
		});
		
		// Listening to Peers button click
		btn_peers.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching Available Peers
				Intent i = new Intent(getApplicationContext(),
						Chat1Activity.class);
				startActivity(i);
			}
		});

		
		// Listening to GPS Tracker button click
		btn_gps.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching GPS Tracker 
				Intent i = new Intent(getApplicationContext(),
						GPSTrackerActivity.class);
				startActivity(i);
			}
		});
		
		// Listening to Camera button click
		btn_camera.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching Camera  
				Intent i = new Intent(getApplicationContext(),
						CameraActivity.class);
				startActivity(i);
			}
		});


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
