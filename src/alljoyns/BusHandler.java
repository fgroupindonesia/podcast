package alljoyns;

import main.layout.Chat1Activity;

import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusListener;
import org.alljoyn.bus.Mutable;
import org.alljoyn.bus.ProxyBusObject;
import org.alljoyn.bus.SessionListener;
import org.alljoyn.bus.SessionOpts;
import org.alljoyn.bus.SessionPortListener;
import org.alljoyn.bus.Status;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* This class will handle all AllJoyn calls. See onCreate(). */
public class BusHandler extends Handler {

	private Chat1Activity obChat1Activity;

	private static final String SERVICE_NAME = "alljoyns.simpleservice";
	private static final String PACKAGE_NAME = "alljoyns";
	private static final short CONTACT_PORT = 42;

	private BusAttachment mBus;
	private ProxyBusObject mProxyObj;
	private MyServiceInterface mSimpleInterface;
	private ServiceImplementator mInterface = new ServiceImplementator();

	private int mSessionId;
	private boolean mIsInASession;
	private boolean mIsConnected;
	private boolean mIsStoppingDiscovery;

	/* These are the messages sent to the BusHandler from the UI. */
	public static final int CONNECT = 1;
	public static final int JOIN_SESSION = 2;
	public static final int DISCONNECT = 3;
	public static final int PING = 4;

	private String sName;
	private short sTransport;

	public void setActivity(Chat1Activity obj) {
		obChat1Activity = obj;
	}

	public BusHandler(Looper looper, Chat1Activity obj) {
		super(looper);
		setActivity(obj);
		mIsInASession = false;
		mIsConnected = false;
		mIsStoppingDiscovery = false;
	}

	public void advertiseAndDiscover() {

		org.alljoyn.bus.alljoyn.DaemonInit.PrepareDaemon(obChat1Activity
				.getApplicationContext());

		mBus = new BusAttachment(PACKAGE_NAME,
				BusAttachment.RemoteMessage.Receive);

		mBus.registerBusListener(new BusListener());

		Status status = mBus.registerBusObject(mInterface, "/SimpleService");

		if (status != Status.OK) {
			obChat1Activity.sendMessage("Something here");
			return;
		}

		status = mBus.connect();

		if (status != Status.OK) {
			obChat1Activity.sendMessage("Connecting 1st failed!");
			return;
		}

		Mutable.ShortValue contactPort = new Mutable.ShortValue(CONTACT_PORT);

		SessionOpts sessionOpts = new SessionOpts();
		sessionOpts.traffic = SessionOpts.TRAFFIC_MESSAGES;
		sessionOpts.isMultipoint = false;
		sessionOpts.proximity = SessionOpts.PROXIMITY_ANY;

		sessionOpts.transports = SessionOpts.TRANSPORT_ANY
				+ SessionOpts.TRANSPORT_WFD;

		status = mBus.bindSessionPort(contactPort, sessionOpts,
				new SessionPortListener() {
					@Override
					public boolean acceptSessionJoiner(short sessionPort,
							String joiner, SessionOpts sessionOpts) {
						if (sessionPort == CONTACT_PORT) {
							return true;
						} else {
							return false;
						}
					}
				});

		if (status != Status.OK) {
			obChat1Activity.sendMessage("Binding incomplete!");
			return;
		}

		int flag = BusAttachment.ALLJOYN_REQUESTNAME_FLAG_REPLACE_EXISTING
				| BusAttachment.ALLJOYN_REQUESTNAME_FLAG_DO_NOT_QUEUE;

		status = mBus.requestName(SERVICE_NAME, flag);

		if (status == Status.OK) {

			status = mBus.advertiseName(SERVICE_NAME, sessionOpts.transports);

			if (status != Status.OK) {
				status = mBus.releaseName(SERVICE_NAME);
				obChat1Activity.sendMessage("Eliminated!");
				return;
			}
		}

	}

	public void connect() {

		org.alljoyn.bus.alljoyn.DaemonInit.PrepareDaemon(obChat1Activity
				.getApplicationContext());

		mBus = new BusAttachment(PACKAGE_NAME,
				BusAttachment.RemoteMessage.Receive);

		mBus.registerBusListener(new BusListener() {
			@Override
			public void foundAdvertisedName(String name, short transport,
					String namePrefix) {

				if (!mIsConnected) {
					sTransport = transport;
					sName = name;
					joinSession();
				}
				
				obChat1Activity.sendMessage("We've found it " + sName);
				
			}
		});

		Status status = mBus.connect();

		if (Status.OK != status) {
			obChat1Activity.sendMessage("Another wrongthing happened!");
			return;
		}

		status = mBus.findAdvertisedName(SERVICE_NAME);

		if (Status.OK != status) {
			obChat1Activity.sendMessage("Oops!");
			return;
		}

	}

	public void joinSession() {

		if (mIsStoppingDiscovery) {
			return;
		}

		short contactPort = CONTACT_PORT;
		SessionOpts sessionOpts = new SessionOpts();
		sessionOpts.transports = (short) sTransport;
		Mutable.IntegerValue sessionId = new Mutable.IntegerValue();

		Status status = mBus.joinSession((String) sName, contactPort,
				sessionId, sessionOpts, new SessionListener() {
					@Override
					public void sessionLost(int sessionId, int reason) {
						mIsConnected = false;
						obChat1Activity.sendMessage("Session is lost");
					}
				});

		if (status == Status.OK) {

			mProxyObj = mBus.getProxyBusObject(SERVICE_NAME, "/SimpleService", sessionId.value, new Class<?>[] { MyServiceInterface.class });
			
			mSimpleInterface = mProxyObj.getInterface(MyServiceInterface.class);

			mSessionId = sessionId.value;
			mIsConnected = true;
			obChat1Activity.sendMessage("Stop please!");
		}
		return;

	}

	public void disconnect() {

		mIsStoppingDiscovery = true;
		if (mIsConnected) {
			Status status = mBus.leaveSession(mSessionId);
			// logStatus("BusAttachment.leaveSession()", status);
		}

		mBus.disconnect();
		getLooper().quit();

	}

	public void ping() {

		try {
			if (mSimpleInterface != null) {
				obChat1Activity.sendMessage("Something", sName);
				String reply = mSimpleInterface.Ping((String) sName);
				obChat1Activity.sendMessage("A Reply", reply);
			}
		} catch (BusException ex) {
			System.err.print("SimpleInterface.Ping() " + ex);
		}

	}

	@Override
	public void handleMessage(Message msg) {

	}

}
