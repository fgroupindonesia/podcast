package alljoyns;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusMethod;
import org.alljoyn.bus.annotation.Secure;

@BusInterface(name="alljoyn.myservice")
@Secure(value="off")
public interface MyServiceInterface {

	@BusMethod
    String Ping(String inStr) throws BusException;
	
}
