package alljoyns;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusObject;

public class ServiceImplementator implements MyServiceInterface, BusObject {

	@Override
	public String Ping(String inStr) throws BusException {
		return "You said " + inStr; 
	}

}
